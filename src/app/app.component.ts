import { Component, OnInit, Input } from '@angular/core';
import { IndicatorsService } from './services/indicators/indicators.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.sass']
})
export class AppComponent implements OnInit {
  title = 'test-bice-angular';

  public indicatorInput: string;
  public qtyInput: number;
  public indicatorOutput: string;

  public indicators:any = [];
  public indicatorsList = [];
  public result:string;
  private dolarPesos:number;

  constructor(
    private _indicatorsService: IndicatorsService
) { }

  ngOnInit() {
    this.printView();
  }

  printView() {
    this._indicatorsService.get().subscribe(
      (res:any) => {
        this.indicators = res;

        for(let item in res){
          if( res[item].unit === 'pesos' || res[item].unit === 'dolar' ){
            this.indicatorsList.push(res[item].key);
          }
        }

        this.dolarPesos = this.indicators['dolar'].value;
      },
      () => {
        console.error('ERROR?');
      }
    );


  }

  calculator(){
    if( this.qtyInput && this.indicatorInput && this.indicatorOutput ){

      let input:any;
      let output:any;

      input = this.indicators[this.indicatorInput];
      output = this.indicators[this.indicatorOutput];

      let subTotal = 0;
      let total = 0;
      let inputPesosValue = input.unit === 'dolar' ? input.value * this.dolarPesos : input.value;
      let outputPesosValue = output.unit === 'dolar' ? output.value * this.dolarPesos : output.value;
      
      subTotal = this.qtyInput * inputPesosValue;
      total = parseFloat((subTotal / outputPesosValue).toFixed(3));

      this.result = `El resultado es: ${ this.qtyInput } [${ this.indicatorInput }] es igual a ${ total } [${ this.indicatorOutput }] *${output.name}`;
    }
    else{
      this.result = `(Debes seleccionar: cantidad, indicador de entrada y salida para poder calcular)`;
    }
  }
}

