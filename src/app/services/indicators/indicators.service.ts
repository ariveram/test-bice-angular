import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class IndicatorsService {

  constructor(private _http: HttpClient) { }

  public get() {
    const url = `http://localhost:3000/indicators`;
    return this._http.get(url);
  }
}