# TestBiceAngular

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 7.3.8.

## Development server

Entrar a la carpeta `cd test-bice-angular`.
Ejecutar `npm i` para instalación de modulos y dependencias.
Ejecutar `ng serve --open` para levantar servidor de desarrollo. El navegador se abrira de forma automática en la dirección `http://localhost:4200/`.